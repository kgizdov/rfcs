============================
Rename the Trusted User role
============================

- Date proposed: 2021-10-07
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/7

Summary
-------

It was shown in some cases that the Trusted User (TU) role naming led to some
confusion and misunderstanding in broader contexts outside Arch Linux.
Furthermore, it has been discussed that it also resulted in similar confusion
among members of Arch Linux internally. This RFC remedies that with
a better-suited term to reflect more accurately upon TUs' roles and
responsibilities as well as their position in the Arch Linux Organisation.

Motivation
----------

There have been several discussions among Arch Linux members both on IRC
and on the staff@lists.archlinux.org mailing list [1]_ about the
"Trusted User" naming (later referred to simply by "the naming" or
"the name") and its associated controversies.

Some relevant points of the discussion:

- The naming, i.e. "Trusted User", puts an unnecessary boundary between
  Trusted Users and the rest of Arch Linux Staff, sometimes purely
  conceptually but sometimes proved pivotally a burden. It suggests
  Trusted Users are not full members of the project but external actors
  purely related by a trust-only relationship to the rest of the staff.
  While this is important during the application process, it becomes
  detrimental to developing more meaningful and closer relationships
  between team members.
- The name should represent being a full member of and having a stake
  in Arch Linux.
- The name is also confusing when interpreted publicly by third parties,
  e.g. on CVs, public profiles and pages. The naming cannot be directly
  understood as a position with real work and responsibilities involved.
- There are also situations where the naming cannot represent the
  contribution/work made by TUs themselves very well. TUs are shown as
  the "user" of such work rather than the main body involved in the R&D
  and implementation.
- The name impedes conveying the due diligence needed to become a TU:
  the public tend to assume that a TU is just a user that happens to
  know an Arch Linux Developer in person.

Specification
-------------

The "Trusted User" role is hereby renamed to "Package Maintainer" as is. As such,
all Arch Linux Trusted Users would become Arch Linux Package Maintainers.
No other changes about the position in question are included in this proposal.

Drawbacks
---------

The simple name change should have minimal impact internally and externally
for Arch Linux. In terms of their responsibilities and descriptions,
the roles remain unchanged making this the minimal incremental change
given the ideas discussed (Sections `Motivation`_ and `Alternatives Considered`_).
Possible difficulties exist in amending the legal documentation and making
sure all relevant Wiki and other public and private information is updated
timely to mitigate any confusion.

However, there is a the partial naming overlap with the role of ArchWiki Maintainer [2]_
where this proposal will have Trusted User be fully read as Arch Linux Package Maintainer
or simply Package Maintainer. While this could lead to partial ambiguity it will address
many more and more serious issues discussed in section `Motivation`_.

It should also be noted that this change should be allowed to propagate
internally and externally for enough time so that members and the public
become accustomed to the naming before allowing any further re-structuring
of the relevant roles. The drawback here is that any more considerable role
re-design will have to be delayed by some time. Judging by previous such
changes with significant impact, Arch Linux has usually provided about 6
months for everyone to adjust.

Unresolved Questions
--------------------

There should be no specific outstanding and unresolved questions. The new
naming has been chosen based on a semi-broad set of comments from TUs and
other Arch Linux members both on IRC and the mailing list. The broader
non-member community of Arch Linux and the public should possibly have
no impact on this decision. Unless, of course, there is a specific reason
raised to re-think this point.

Alternatives Considered
-----------------------

There have been multiple other suggestions that have been considered before [1]_
the above selection in the `Specification`_ section, e.g.:

- Member
- Maintainer
- Proponent
- Paladin

Furthermore, there have been ideas towards renaming the role of "Developer"
to "Core Maintainer", the role of "Trusted User" to simply "Maintainer" and
creating a new role called "Developer" solely reserved for members of Arch
Linux who do code development work directly relevant to Arch Linux [3]_.

Another similar idea was to keep the role structure as is but rename the
current "Developer" role into "Core Developer" and re-labelling the
"Trusted User" role to "Developer" [4]_ [5]_.

Also, this proposal has been seen by some as the first step towards
a larger, more meaningful re-design as so ("->" is to be read as "becomes") [6]_:

- package maintenance, in [core]/[extra] (Developer only)
  and [community] (everyone, especially TU) -> (Core) Maintainer
- sysadmin (including dedicated tooling), which is partly dev but not only,
  and even non-TU -> Devops (currently in Support Staff)
- tooling development, be it pacman, archweb, devtools -> Developers
- Wiki, forum, IRC/Matrix, bug wrangler, (...) -> Support Staff
- AUR handling, especially PRQ -> Trusted Users

Finally, a two-round-system vote was held at this RFC's MR discussion thread
to chose the final naming scheme proposed here. It consisted initially of all
popular labels and then kept only the two most popular options in the final
round to achieve majority.

.. [1] https://lists.archlinux.org/private/staff/2021-July/000673.html
.. [2] https://wiki.archlinux.org/title/ArchWiki:Maintenance_Team
.. [3] https://lists.archlinux.org/private/staff/2021-July/000702.html
.. [4] https://lists.archlinux.org/private/staff/2021-July/000678.html
.. [5] https://lists.archlinux.org/private/staff/2021-July/000705.html
.. [6] https://lists.archlinux.org/private/staff/2021-July/000687.html
